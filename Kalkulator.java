package kalkulator;
import java.util.*;
public class Kalkulator {
    public static void main(String args[]) throws java.io.IOException {
        double db1 = 0, db2 = 0;
        int choice = 0;
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("������� ������ �����:");
                db1 = (double) sc.nextDouble();
            } catch (InputMismatchException exc) {
                System.out.println("\"" + sc.nextLine() + "\" �� �������� ������" + "\n");
                continue;
            }
            do {
                try {
                    System.out.println("�������� �������� ��������:");
                    System.out.println("1 - ��������\n2 - ���������\n3 - ���������\n4 - �������");
                    choice = (int) sc.nextInt();
                } catch (InputMismatchException exc) {
                    System.out.println("\"" + sc.nextLine() + "\" �� �������� ������" + "\n");
                    continue;
                }
                try {
                    System.out.println("������� ������ �����:");
                    db2 = (double) sc.nextDouble();
                } catch (InputMismatchException exc) {
                    System.out.println("\"" + sc.nextLine() + "\" �� �������� ������" + "\n");
                    continue;
                }
                switch (choice) {
                    case 1:
                        System.out.println(db1 + " + " + db2 + " = " + (db1 + db2) + "\n");
                        break;
                    case 2:
                        System.out.println(db1 + " - " + db2 + " = " + (db1 - db2) + "\n");
                        break;
                    case 3:
                        System.out.println(db1 + " * " + db2 + " = " + (db1 * db2) + "\n");
                        break;
                    case 4:
                        try {
                            if (db2 == 0) {
                                throw new ArithmeticException("1");
                            } else {
                                System.out.println(db1 + " / " + db2 + " = " + (db1 / db2) + "\n");
                            }
                        } catch (ArithmeticException exp) {
                            System.out.println("������:������� �� 0");
                            continue;
                        }
                }
            } while (choice < 1 | choice > 4);
        }
    }
}